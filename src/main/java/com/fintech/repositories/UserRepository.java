package com.fintech.repositories;

import com.fintech.entities.UserEntity;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface UserRepository extends ReactiveMongoRepository<UserEntity, String> {
    Mono<UserEntity> findByEmail(String email);

    Mono<UserEntity> findByEmailAndPassword(String email, String password);
}
