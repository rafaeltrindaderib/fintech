package com.fintech.repositories;

import com.fintech.entities.TransactionEntity;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface TransactionRepository extends ReactiveMongoRepository<TransactionEntity, Integer> {
}
