package com.fintech.repositories;

import com.fintech.entities.AccountEntity;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface AccountRepository extends ReactiveMongoRepository<AccountEntity, String> {
}
