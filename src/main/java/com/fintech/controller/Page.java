package com.fintech.controller;

import com.fintech.dto.auth.Login;
import com.fintech.dto.user.CreateUser;
import com.fintech.service.AuthService;
import com.fintech.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.server.WebSession;
import reactor.core.publisher.Mono;
import org.springframework.web.reactive.result.view.Rendering;

@Controller
public class Page {

    @Autowired
    private AuthService authService;

    @Autowired
    private UserService userService;

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("login", new Login("", ""));
        model.addAttribute("user", new User());
        return "index";
    }

    @PostMapping("/login")
    public Mono<Rendering> login(@ModelAttribute Login login, Model model, WebSession session) {
        return authService.login(login)
                .flatMap(authenticated -> {
                    if (authenticated) {
                        session.getAttributes().put("user", login.email());
                        return Mono.just(Rendering.redirectTo("/").build());
                    } else {
                        model.addAttribute("error", "Invalid email or password");
                        model.addAttribute("login", new Login("", ""));
                        model.addAttribute("user", new User());
                        return Mono.just(Rendering.view("index").build());
                    }
                });
    }

//    @PostMapping("/user-form")
//    public String createUser(@ModelAttribute User user, Model model) {
//        userService.createUser(new CreateUser(user.name(), user.email())).block();
//        return "redirect:/";
//    }

    @GetMapping("/user-form")
    public String userForm(Model model) {
        model.addAttribute("user", new User());
        return "user-form";
    }
}
