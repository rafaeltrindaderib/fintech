package com.fintech.controller;

import com.fintech.entities.TransactionEntity;
import com.fintech.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/transacoes")
public class Transaction {
    @Autowired
    private TransactionService transactionService;

    @PostMapping("/depositar")
    public Mono<TransactionEntity> depositar(@RequestParam String contaId, @RequestParam Float valor) {
        return transactionService.depositar(contaId, valor);
    }

    @PostMapping("/sacar")
    public Mono<TransactionEntity> sacar(@RequestParam String contaId, @RequestParam Float valor) {
        return transactionService.sacar(contaId, valor);
    }

    @PostMapping("/transferir")
    public Mono<TransactionEntity> transferir(@RequestParam String contaOrigemId, @RequestParam String contaDestinoId, @RequestParam Float valor) {
        return transactionService.transferir(contaOrigemId, contaDestinoId, valor);
    }
}
