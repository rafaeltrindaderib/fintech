package com.fintech.controller;

import com.fintech.dto.auth.Login;
import com.fintech.dto.user.CreateUser;
import com.fintech.entities.UserEntity;
import com.fintech.service.AuthService;
import com.fintech.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/auth")
public class Auth {

    @Autowired
    AuthService authService;
    @PostMapping
    public Mono<Boolean> login (@RequestBody Login data){
        return authService.login(data);
    }

}
