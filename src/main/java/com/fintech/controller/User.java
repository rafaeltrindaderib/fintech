package com.fintech.controller;

import com.fintech.dto.user.CreateUser;
import com.fintech.dto.user.UpdateUser;
import com.fintech.entities.UserEntity;
import com.fintech.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController

@RequestMapping("/user")
public class User {
    @Autowired
    UserService userService;
    @PostMapping
    public Mono<UserEntity> createUser (@RequestBody CreateUser data){
        return userService.createUser(data);
    }

    @GetMapping
    public Flux<UserEntity> getAll (){
        return userService.getAll();
    }

    @PutMapping
    public Mono<UserEntity> updateUser(@RequestBody UpdateUser data) {return userService.update(data);}

}
