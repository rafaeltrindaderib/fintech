package com.fintech.service;

import com.fintech.dto.user.CreateUser;
import com.fintech.dto.user.UpdateUser;
import com.fintech.entities.AccountEntity;
import com.fintech.entities.AccountType;
import com.fintech.entities.UserEntity;
import com.fintech.factory.UserFactory;
import com.fintech.repositories.UserRepository;
import com.fintech.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Random;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AccountRepository accountRepository;

    public Mono<UserEntity> createUser(CreateUser data) {
        UserEntity dataCasted = UserFactory.createUserEntity(data);
        return userRepository.findByEmail(dataCasted.getEmail()).flatMap(
                u -> Mono.<UserEntity>error(new RuntimeException("User already exists"))
        ).switchIfEmpty(Mono.defer(() -> {
            return userRepository.insert(UserFactory.createUserEntity(data))
                    .flatMap(user -> {
                        // Criar conta após a criação do usuário
                        AccountEntity account = new AccountEntity();
                        account.setNumber(generateAccountNumber());
                        account.setUserId(user.getId());
                        account.setSaldo(BigDecimal.ZERO);
                        account.setType(AccountType.CHECKING); // ou AccountType.INVESTMENT
                        account.setCreatedAt(LocalDateTime.now());
                        account.setUpdatedAt(LocalDateTime.now());

                        return accountRepository.save(account)
                                .thenReturn(user);
                    });
        }));
    }

    public Flux<UserEntity> getAll() {
        return userRepository.findAll();
    }

    public Mono<UserEntity> update(UpdateUser data) {
        return userRepository.findById(data.id())
                .flatMap(existingUser -> {
                    existingUser.setName(data.name());
                    existingUser.setEmail(data.email());
                    existingUser.setPassword(data.password());
                    existingUser.setCpfCnpj(data.cpfCnpj());
                    return userRepository.save(existingUser);
                });
    }

    private String generateAccountNumber() {
        Random random = new Random();
        StringBuilder accountNumber = new StringBuilder();
        for (int i = 0; i < 10; i++) {
            accountNumber.append(random.nextInt(10));
        }
        return accountNumber.toString();
    }
}
