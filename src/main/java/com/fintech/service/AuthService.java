package com.fintech.service;

import com.fintech.dto.auth.Login;
import com.fintech.entities.UserEntity;
import com.fintech.factory.UserFactory;
import com.fintech.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class AuthService {
    @Autowired
    private UserRepository userRepository;

    public Mono<Boolean> login (Login data) {

        return userRepository.findByEmailAndPassword(data.email(), data.password())
                .flatMap(u -> Mono.just(true))
                .switchIfEmpty(Mono.just(false));
    }

    public Flux<UserEntity> getAll() {
        return userRepository.findAll();
    }
}
