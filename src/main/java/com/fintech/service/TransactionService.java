package com.fintech.service;

import com.fintech.entities.TransactionEntity;
import com.fintech.entities.AccountEntity;
import com.fintech.repositories.TransactionRepository;
import com.fintech.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Service
public class TransactionService {
    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private AccountRepository accountRepository;

    public Mono<TransactionEntity> depositar(String contaId, Float valor) {
        return accountRepository.findById(contaId)
                .flatMap(conta -> {
                    conta.setSaldo(conta.getSaldo().add(BigDecimal.valueOf(valor)));
                    return accountRepository.save(conta)
                            .flatMap(savedConta -> {
                                TransactionEntity transaction = new TransactionEntity();
                                transaction.setContaOrigem(contaId);
                                transaction.setContaDestino(null);
                                transaction.setValor(valor);
                                transaction.setDataHora(LocalDateTime.now());
                                transaction.setTipo("DEPÓSITO");
                                return transactionRepository.save(transaction);
                            });
                });
    }

    public Mono<TransactionEntity> sacar(String contaId, Float valor) {
        return accountRepository.findById(contaId)
                .flatMap(conta -> {
                    if (conta.getSaldo().compareTo(BigDecimal.valueOf(valor)) >= 0) {
                        conta.setSaldo(conta.getSaldo().subtract(BigDecimal.valueOf(valor)));
                        return accountRepository.save(conta)
                                .flatMap(savedConta -> {
                                    TransactionEntity transaction = new TransactionEntity();
                                    transaction.setContaOrigem(contaId);
                                    transaction.setContaDestino(null);
                                    transaction.setValor(valor);
                                    transaction.setDataHora(LocalDateTime.now());
                                    transaction.setTipo("SAQUE");
                                    return transactionRepository.save(transaction);
                                });
                    }
                    return Mono.error(new RuntimeException("Saldo insuficiente"));
                });
    }

    public Mono<TransactionEntity> transferir(String contaOrigemId, String contaDestinoId, Float valor) {
        return accountRepository.findById(contaOrigemId)
                .flatMap(contaOrigem -> {
                    if (contaOrigem.getSaldo().compareTo(BigDecimal.valueOf(valor)) >= 0) {
                        contaOrigem.setSaldo(contaOrigem.getSaldo().subtract(BigDecimal.valueOf(valor)));
                        return accountRepository.save(contaOrigem)
                                .flatMap(savedContaOrigem -> accountRepository.findById(contaDestinoId)
                                        .flatMap(contaDestino -> {
                                            contaDestino.setSaldo(contaDestino.getSaldo().add(BigDecimal.valueOf(valor)));
                                            return accountRepository.save(contaDestino)
                                                    .flatMap(savedContaDestino -> {
                                                        TransactionEntity transaction = new TransactionEntity();
                                                        transaction.setContaOrigem(contaOrigemId);
                                                        transaction.setContaDestino(contaDestinoId);
                                                        transaction.setValor(valor);
                                                        transaction.setDataHora(LocalDateTime.now());
                                                        transaction.setTipo("TRANSFERÊNCIA");
                                                        return transactionRepository.save(transaction);
                                                    });
                                        }));
                    }
                    return Mono.error(new RuntimeException("Saldo insuficiente"));
                });
    }
}
