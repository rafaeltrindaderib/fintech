package com.fintech.entities;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document(collection = "user")
@Data
public class UserEntity {
    @Id
    private String id;
    private String name;
    private String email;
    private String password;
    private String cpfCnpj;

    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
}
