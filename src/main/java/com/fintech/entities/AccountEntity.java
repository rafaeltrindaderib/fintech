package com.fintech.entities;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.time.LocalDateTime;


@Document(collection = "account")
@Data
public class AccountEntity {
    @Id
    private String id;
    private String number;
    private String userId;  // Reference to UserEntity
    private BigDecimal saldo;
    private AccountType type;

    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
}
