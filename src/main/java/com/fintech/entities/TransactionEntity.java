package com.fintech.entities;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document(collection = "transacao")
@Data
public class TransactionEntity {
    @Id
    private String id;
    private String contaOrigem;
    private String contaDestino;
    private Float valor;
    private LocalDateTime dataHora;
    private String tipo;
}
