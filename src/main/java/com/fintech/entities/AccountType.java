package com.fintech.entities;

public enum AccountType {
    CHECKING,
    INVESTMENT
}