package com.fintech.dto.user;

import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

public record CreateUser (
        @Id
        String id,
        String name,
        String email,
        String password,
        String cpfCnpj
) {
}
