package com.fintech.dto.auth;

import java.time.LocalDateTime;

public record Login (

        String email,
        String password

) {
}
