package com.fintech.factory;

import com.fintech.dto.user.CreateUser;
import com.fintech.entities.UserEntity;

import java.time.LocalDateTime;

public class UserFactory {

    public static UserEntity createUserEntity(CreateUser user){
        UserEntity userEntity = new UserEntity();
        userEntity.setCpfCnpj(user.cpfCnpj());
        userEntity.setName(user.name());
        userEntity.setEmail(user.email());
        userEntity.setPassword(user.password());
        userEntity.setCreatedAt(LocalDateTime.now());
        userEntity.setUpdatedAt(LocalDateTime.now());

        return userEntity;
    }
}
